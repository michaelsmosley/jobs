/**
 * Module dependencies.
 */
var request = require('request')
  , express = require('express')
  , http = require('http')
  , path = require('path');

//Create an express app
var app = express();

//Create the HTTP server with the express app as an argument
var server = http.createServer(app);
var linearray=[];


//Generic Express setup
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

//We're using bower components so add it to the path to make things easier
app.use('/components', express.static(path.join(__dirname, 'components')));


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//Create the server
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

var triangledataurl='http://www.yodlecareers.com/puzzles/triangle.txt';
//var triangledataurl='https://raw.github.com/loisaidasam/yodle/master/triangle/triangle_test.txt';
//var triangledataurl='http://localhost:3000/triangle_test.txt';


request(triangledataurl, function (error, response, body) {
  if (!error && response.statusCode == 200) {
  	var data = body.split("\n");
   	//BUILD ARRAYS
    data.forEach(function(entry) {
    	entry = entry.replace(/(\r\n|\n|\r)/gm,"");
    	var line = entry.split(" ");
    	
    	if (line[line.length-1]=='') {
    		line.pop();
    	}
    	linearray.push(line);
	});

   var maxval=prevrow();

/*SLOW WAY -- WAY TOO SLOW
   var maxval=nextrow(0,0,0);
   //console.log("maxval next"+maxval);

*/
    app.get('/', function(req, res) {
		res.render('index', { triangleanswer: maxval });
	});
	
  }
})
function nextrow (row,column,pathtotal) {
  var line =linearray[row];
  pathtotal+=Number(line[column]);
  if (row==linearray.length-1) {
      return pathtotal;
  }

  var option_1 = nextrow(row+1, column, pathtotal);
  var option_2 = nextrow(row+1, column+1, pathtotal);
  if (option_1 > option_2) {
    return option_1;
  } else {
    return option_2;
  }
}

function prevrow() {
            var testval;
            var linearraylength=linearray.length;
            var pathtotal=0;
            for (var i =  linearraylength- 2; i>0; i--) {

                var newline=[];
                for(j = 0; j<=i; j++) {
                        testval =nextrow(i,j,0);
                        //console.log("testval +"+testval);
                        newline.push(testval);
                }
                linearray[i]=newline;
                linearray.pop();
                //console.log("now linearray[i]"+linearray[i])  
                
            }


            
            
            var initnumber=Number(linearray[0][0]);
            //console.log("initnumber +"+initnumber);
            return testval+initnumber;

}